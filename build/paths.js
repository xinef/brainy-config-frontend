var appRoot = 'src/';
var outputRoot = 'dist/';
var exportSrvRoot = 'export/';

module.exports = {
  root: appRoot,
  source: appRoot + '**/*.js',
  test: 'test/**/*.js',
  html: appRoot + '**/*.html',
  style: 'styles/**/*.scss',
  output: outputRoot,
  exportSrv: exportSrvRoot
};
